package com.wuttiwat.bmi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import java.text.NumberFormat
import com.wuttiwat.bmi.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        fun displayBmi() {
            binding.bmiResult.text = "0"
            binding.bmiText.text = ""
        }

        displayBmi()
        binding.calculateButton.setOnClickListener { calculateBmi() }
    }

    private fun displayBmi(){
        binding.bmiResult.text = "0"
        binding.bmiText.text = ""
    }


    private fun calculateBmi() {
        val stringInTextField = binding.heavyBody.text.toString()
        val heavy = stringInTextField.toDoubleOrNull()
        val stringInTextField1 = binding.heightBody.text.toString()
        val height = stringInTextField1.toDoubleOrNull()
        if (heavy == null || height == null) {
            displayBmi()
            return
        }
        else{
            val height1 = height/100
            var bmi = heavy/(height1*height1)
            val roundedNumber = "%.2f".format(bmi)
            binding.bmiResult.text = roundedNumber.toString()
            bmi = kotlin.math.ceil(bmi)

            when{
                bmi < 18.50 -> binding.bmiText.text = "under"
                bmi in 18.50..22.90 -> binding.bmiText.text = "nomal"
                bmi in 23.00..24.90 -> binding.bmiText.text = "risk"
                bmi in 25.00..29.90 -> binding.bmiText.text = "over"
                else -> binding.bmiText.text = "obese"
            }
        }

    }
}